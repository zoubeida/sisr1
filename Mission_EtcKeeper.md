## Mission : Installation de l'outil EtcKeeper 
[Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), Hautil
Bloc2 SISR1 (BTS - SIO) 01/02/2021 (modifié 22/03/2023)




## Objectif


L'objectif de cette activité est d'apprendre à installer l'outil EtcKeeper sur une machine Debian et à l'utiliser.

A déposer sur GitLab sous le nom : `SISR1/Mission_Etckeeper.md` et `SISR1/Mission_Etckeeper.pdf`

Voici quelques sites de référence :

- <https://wiki.archlinux.org/index.php/etckeeper>
- <https://etckeeper.branchable.com/>
- <https://nos-oignons.net/wiki-admin/Services/Debian/etckeeper/>
- <https://www.digitalocean.com/community/tutorials/how-to-manage-etc-with-version-control-using-etckeeper-on-centos-7>

## Etape 1 - Préparation d'une nouvelle machine

Clôner votre machine virtuelle initiale Debian créée à la **Mission1**.

- Noter le type de configuration réseau choisi avec votre hyperviseur
- Mettre à jour la nouvelle VM. 

## Etape 2 - L'outil EtcKeeper ?

On aimerait installer l'outil EtcKeeper sur notre nouvelle machine virtuelle.

- Nommer plusieurs utilités à ce nouvel outil.
- Quelles fonctionnalités sont possibles avec cet outil ?
- Est-il nécessaire de l'installer systématiquement sur chaque nouvelle machine ? Pourquoi ?

## Etape 3 - Installation de l'outil EtcKeeper

- Vérifier si l'outil EtcKeeper n'est pas déjà installé sur la machine. Noter la commande, la réponse et son interprétation.
- Si EtcKeeper n'est pas installé, l'installer. Noter la commande.
- Quel est le fichier de configuration d'EtcKeeper ? Noter le chemin complet de ce fichier.  
- Noter les commandes utiles pour mettre en oeuvre cet outil.
- Initialiser l'outil. Noter la commande.


## Etape 4 - Mise en oeuvre d'une opération d'administration

Ne pas oublier de noter toutes les commandes opérées et leur résultat(image).

1. Opérer une première validation des changements (via etckeeper).
2. Lister les différentes validations réalisées (via etckeeper).

3. Installer un serveur SSH avec l'outil graphique `tasksel` 
	- lister les changements opérés après l'installation du serveur  (grâce à etckeeper)
	- valider les modifications  (grâce à etckeeper)
4. Créer un nouvel utilisateur toto
	- Utiliser la commande `useradd`. 
	- Renseigner le mot de passe de votre utilisateur (mdp voué à changer)
	- lister les changements opérés après l'installation du serveur (grâce à etckeeper). 
	- valider les modifications  (grâce à etckeeper)
5. Tester votre nouvel utilisateur créé 
	- Accéder avec le nouvel utilisateur à votre machine virtuelle 
	- tenter un accès via ssh avec ce nouvel utilisateur

6. Mauvais nom d'utilisateur, revenir en arrière.
	- tester que l'utilisateur toto n'existe plus. Noter les procédures de test !