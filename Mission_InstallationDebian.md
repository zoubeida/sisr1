## Mission1 : Installation Debian sur Machine Virtuelle
[Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), Hautil
Bloc2 SISR1 (BTS - SIO) 04/01/2023






## Objectif


L'objectif de cette activité est d'apprendre à créer une machine virtuelle Debian (installation et configuration) et à créer une documentation utilisateur qui recence toutes les étapes effectuées (en markdown et pdf).

A déposer sur GitLab sous le nom : `SISR1/Mission_DebianInstall.md` et `SISR1/Mission_DebianInstall.pdf`

Voici quelques sites de référence :

- <https://www.virtualbox.org/wiki/Downloads>

- <http://www.tuto-it.fr/virtualisation.php>

- <http://dadidoum.blogspot.fr/2012/03/tuto-installation-de-debian-sous.html>



## Etape 1
Installer VirtualBox, logiciel d'hypervision.

- Identifier la version installée et utilisée. 
- Identifier le Système d'exploitation hôte.
- Rappeler les étapes pour accéder au BIOS (UFI) pour vérifier le paramètre de configuration qui permet de rendre la virtualisation possible.
- Identifier le type de virtualisation mis en oeuvre par l'hyperviseur VirtualBox.

## Etape 2
Créer une première machine virtuelle avec un système d'exploitation Linux Debian.

- Identifier la version téléchargée et installée. 
- Repérer les différentes étapes importantes de l'installation (captures d'écran à l'appuie si nécessaire)
- Noter le login/mdp de votre utilisateur principal (`root` sans mot de passe)


## Etape 3
Configurer le réseau de sorte que la machine puisse avoir accès au réseau internet accessible par la machine hôte.

- Noter la configuration nécessaire pour permettre à votre machine virtuelle d'accéder à internet (Configuration de l'hyperviseur).
- Noter la procédure utilisée pour **vérifier** l'accès à internet par la machine virtuelle
- Cette configuration permet-elle à la machine hôte d'accéder à la machine virtuelle
- Noter la procédure utilisée pour **vérifier** l'accès à la machine virtuelle par la machine hôte

## Etape 4
Configurer le réseau de sorte que la machine hôte puisse discuter avec sa machine virtuelle et vice-versa

- Identifier la configuration nécessaire pour permettre cette communication bilatérale

## Etape 5 
Mettre à jour la machine avec le gestionnaire de paquet `apt`.

- Que permettent de faire les commandes suivantes : 

	- `apt update` 
	- `apt upgrade`

- Quel fichier du système permet de donner à ces commandes le lien des sites de mise à jour de Debian?