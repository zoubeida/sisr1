## Mission1 : Installation Debian sur Machine Virtuelle
[Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), Hautil
Bloc2 SISR1 (BTS - SIO) 18/01/2023




## Objectif


L'objectif de cette activité est de se familiariser avec le protocole et les outils SSH

## Etape 1 - Préparation d'une nouvelle machine

Clôner votre machine virtuelle initiale Debian créée à la Mission1.

- Noter le type de configuration réseau choisi avec votre hyperviseur
- Explorer le fichier `/etc/apt/sources.list` et ajouter les liens pour permettre d'accéder à tous les paquets potentiels dédiés à la distribution Debian.
- Mettre à jour la nouvelle VM. (Noter les différentes commandes)

## Etape 2 - Un serveur SSH ?

On aimerait installer un serveur SSH sur notre nouvelle machine virtuelle.

- Nommer plusieurs utilités au serveur SSH.
- Quels moyens d'authentification sont possibles pour accéder à un serveur SSH ?
- Quel est le port par défaut d'un serveur SSH ?


## Etape 3 - Installation du serveur SSH

- Vérifier si le serveur SSH n'est pas déjà installé sur la machine. Noter la commande, la réponse et son interprétation.
- Si le serveur n'est pas installé, l'installer. Noter la commande.
- Quel est le fichier de configuration du serveur SSH ? Noter le chemin complet de ce fichier.  
- Noter les commandes qui permettent d'arrêter, démarrer, redémarrer le serveur SSH.
- Démarrer le serveur SSH. Noter la commande pour vérifier son statut de fonctionnement.

## Etape 4 - Test du fonctionnement du serveur SSH

On souhaiterait accéder via la machine hôte, à notre nouvelle machine virtuelle.
 
- Noter la commande qui permettera d'opérer cet accès. 
- Noter les nouvelles configurations nécessaires pour permettre cet accès, s'il y a lieu. 
- Copier dans votre dépôt local Gitlab (sur la machine hôte) le fichier `sources.list` modifié à l'étape 1. Noter la commande.

## Etape 5 - Gestion de paires de clés privée et publique

On souhaiterait se connecter avec une paire de clés privée/public au serveur SSH de la machine virtuelle.

- Noter les étapes nécessaires pour y parvenir.
