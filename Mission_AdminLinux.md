## Mission : Installation Apache2 et administration Système unix
#### [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), Hautil
#### Cours SISR1 (BTS - SIO) - 22/03/2023





## Objectif


L'objectif de cette activité est d'apprendre à administrer une STA (Solution Technique d'Accès) Linux Débian via l'installation d'une serveur web Apache2.

Un **compte rendu d'activités** en Markdown est attendu sur votre dépôt GitLab sous le nom `SISR1/CR_AdminSys.md` et/ou sous le format PDF `SISR1/CR_AdminSys.pdf`.

**ATTENTION :** Ne pas oublier de valider les différentes étapes de votre mission avec etckeeper

Voici quelques sites de référence :

- <http://www.linux-france.org/prj/edu/archinet/systeme/ch16s02.html>
- <http://www.octetmalin.net/linux/tutoriels/chown-changer-proprietaire-groupe-fichier-dossier-repertoire-en-ligne-de-commande.php>
-  <http://forums.cnetfrance.fr/topic/7765-chmod-commande-chmod-linux--unix/>


## Etape 1

Cloner une machine Debian11 légère (1 Go de mémoire vive ou + si possible pour le confort) mise à jour (**etcKeeper et serveur SSH installés**).

Commencer par vérifier l'existence du serveur Apache2 sur la machine virtuelle, sinon l'installer. Tester le bon fonctionnement du serveur.

- Quelle est la ligne de commande nécessaire pour chercher si un paquet débian est déjà installé ? 
- Quelle est la ligne de commande à utiliser pour installer le paquet débian ?
- Quelle est la ligne de commande pour démarrer le serveur ? pour l'arrêter ? ou pour le redémarrer ?

- **Montrer des traces de vos commits avant et après installation d'Apache**

## Etape 2

Il est nécessaire de gérer un groupe d'utilisateurs spécifique à Apache2 : `www-data`. Ce groupe a le droit de lecture uniquement sur les fichiers spécifiques à Apache2.

- Vérifier l'existence de ce groupe. Quelle est la commande réalisée ?
- Qui est le propriétaire des fichiers dans `/var/www` ? 
- Donner les droits d'accès au groupe `www-data` pour voir tous les fichiers de `/var/www`. Quelle est la commande utilisée ?
- Quels droits a le groupe `www-data` sur les fichiers de `/var/www`?  Quelle est la commande utilisée ? 

- **Montrer des traces de vos commits avant et après les modifications opérées**

## Etape 3

Continuer par créer un nouvel utilisateur "lucky".  Cet utilisateur fera parti du groupe `www-data`. Se connecter en tant que lucky ensuite et vérifier l'accès au répertoire `/var/www`.

- Quelle est la commande utilisée pour créer ce nouvel utilisateur ? 
- Quels droits a "lucky" ? peut-il ajouter un nouveau fichier dans `/var/www` ?
- Comment modifier les droits de `www-data` pour lui permettre d'écrire ? Quelle est la commande utilisée ? 
- Créer un fichier `moi.html` dans `/var/www/html` en tant que `www-data` (le fichier contiendra un texte de bienvenue personnel !)


- **Montrer des traces de vos commits avant et après les modifications opérées**

## Etape 4

Configurer le serveur apache pour que chaque utilisateur puisse créer un site HTML via son dossier personnel `public_html`. Modifier les fichiers de configuration en tant que `www-data`.

- Quelles sont les étapes primordiales pour y arriver ? 
- Lister les fichiers de configuration modifiés et les commandes utilisées 
- Tester la nouvelle configuration avec "lucky" et vérifier l'accès à ses pages html via cette adresse `http://localhost/~lucky/moi.html`


- **Montrer des traces de vos commits avant et après les modifications opérées**

## Etape 5
Quand que le serveur est démarré, chercher le processus dans l'arborescence des processus, le tuer et vérifier qu'il est bien arrêté.

- Quelle est la commande utilisée pour voir l'arborescence des processus ? Que peut-on dire du processus apache2 ? 
- Quelle est la commande utilisée pour tuer le processus ? 
- Quelle est la commande utilisée pour vérifier l'arrêt de ce processus ?
