# SISR1

Dépôt des modules du bloc2 : SISR


## Conditions pour un livrable recevable 

- Titre du document, nom des auteurs, date de réalisation.
- Nom de fichier sans espace et avec une extension.
- Images utilisées dans un dossier `img` dédié.

## Progression

**07/12/2022** 

- Mise en route du Bloc2
- Réseau du BTS et fonctionnement de PXE 

**14/12/2022**

- Mise en oeuvre de l'installation des machines de la salle E65 via PXE

- ***Livrable attendu*** : Déposer `Mission_PXE.md` dans le nouveau répertoire du dépôt `SISR1`

- **Certification Cisco** : Compléter le badge "Intro à packet Tracer" avant fin janvier 
   
**04/01/2023** 

- Installation d'une distribution Debian sur une machine virtuelle ou physique. 
    Mission [ici](Mission_InstallationDebian.md) 

**11/01/2023** 

- Installation d'une distribution Debian (suite) et CR à l'appui
 
- ***Livrable attendu*** : Déposer `Mission_DebianInstall.md` dans `SISR1`


**18/01/2023** 

- Installation du serveur SSH : [Mission_SSH](Mission_SSH.md)
